
#Import Necessary modules

#Controls Sensors and time
import grovepi, time

#Controls Input Output pins on raspberry pi
import RPi.GPIO as GPIO

#Created module which moves motors
from Robo import *


#Initialize slots for each sensors
#On grovepi Board
LEFT_SENSOR = 7
RIGHT_SENSOR = 3




#Retrieves distance from left Sensor
def left_sensor():
    global LEFT_SENSOR
    return grovepi.ultrasonicRead(LEFT_SENSOR)


#Retrieves distance from right Sensor
def right_sensor():
    global RIGHT_SENSOR
    return grovepi.ultrasonicRead(RIGHT_SENSOR)


#Continuosly detect distance from each sensors
def sense():
    
    while True:    
        if right_sensor() < 10:
            print("SENSE: RIGHT")
            ROTATE_LEFT()

        if left_sensor() < 10:
            print("SENSE: LEFT")
            ROTATE_RIGHT()

#Run the sensors and if a RuntimeError
#Occurs, wait 5 seconds and run it again
def main():
    
    try:
        sense()

    except RuntimeError:
        time.sleep(5)
        sense()
            
    

            
                    
    
        
