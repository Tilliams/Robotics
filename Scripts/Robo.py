
#Controls Raspberry Pi Pins
import RPi.GPIO as GPIO
import time

#Initialize pins for right wheel
#and left wheel
RIGHT_WHEEL = 11
LEFT_WHEEL = 13

#Set mode to Board so we can use
#Sequential numbers and turn
#off warnings
GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)

#Set up left and right wheels for output
GPIO.setup(RIGHT_WHEEL, GPIO.OUT)
GPIO.setup(LEFT_WHEEL, GPIO.OUT)
GPIO.output(RIGHT_WHEEL, True)
GPIO.output(LEFT_WHEEL, True)





def BACKWARD(frequency = (25, 50)):

    left = GPIO.PWM(LEFT_WHEEL, frequency[1])
    right = GPIO.PWM(RIGHT_WHEEL, frequency[0])

    left.start(5)
    right.start(5)

    time.sleep(1)



def FORWARD(frequency = (25, 50)):
    left = GPIO.PWM(LEFT_WHEEL, frequency[0])
    right = GPIO.PWM(RIGHT_WHEEL, frequency[1])

    left.start(5)
    right.start(5)

    time.sleep(1)

def ROTATE_LEFT(frequency = (25, 50)):

    left = GPIO.PWM(LEFT_WHEEL, frequency[1])
    right = GPIO.PWM(RIGHT_WHEEL, frequency[1])

    left.start(5)
    right.start(5)

    time.sleep(1)
    


def ROTATE_RIGHT(frequency = (25, 50)):

    left = GPIO.PWM(LEFT_WHEEL, frequency[0])
    right = GPIO.PWM(RIGHT_WHEEL, frequency[0])

    left.start(5)
    right.start(5)

    time.sleep(1)

def CLEANUP():
    GPIO.cleanup()

def main():


    
    FORWARD()
    BACKWARD()

    ROTATE_LEFT()
    ROTATE_RIGHT()

    GPIO.cleanup()


if __name__ == '__main__':
    main()
    
